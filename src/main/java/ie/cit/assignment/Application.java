package ie.cit.assignment;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ie.cit.assignment.entity.Artist;
import ie.cit.assignment.entity.Artwork;
import ie.cit.assignment.repository.ArtistRepository;
import ie.cit.assignment.repository.ArtworkRepository;

@SpringBootApplication
public class Application implements CommandLineRunner{
	
	@Autowired
	ArtistRepository artistRepository;
	
	@Autowired
	ArtworkRepository artworkRepository;
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public void run(String... arg0) throws Exception {
		
		String artistFile = arg0[0];
		String artworkFile = arg0[1];
		try {
			
			//File path = new File("C:/Users/Dillon/Desktop/Eclipse/casda/artists");
			/*File path1 = new File("C:/Users/Dillon/Desktop/Eclipse/casda/artwork");
			
			File [] files = path1.listFiles();
			for (int i = 0; i < files.length; i++){
			if (files[i].isFile()){
						Artwork artwork = new ObjectMapper().readValue(files[i], Artwork.class);
						artworkRepository.save(artwork);
					}
			}
			
			File [] files = path.listFiles();
			for (int i = 0; i < files.length; i++){
			if (files[i].isFile()){
						Artist artist = new ObjectMapper().readValue(files[i], Artist.class);
						artistRepository.save(artist);
					}
			}*/
			
			
			System.out.printf("Processing Artist file %s...\n", artistFile);
			Artist artist = new ObjectMapper().readValue(new File(artistFile), Artist.class);
			System.out.println("\n" + artist.toString());
			//artistRepository.save(artist);
			
			
			
			System.out.printf("\nProcessing Artwork File %s...\n", artworkFile);
			Artwork artwork = new ObjectMapper().readValue(new File(artworkFile), Artwork.class);
			System.out.println("\n" + artwork.toString());
			//artworkRepository.save(artwork);

			
			System.out.printf("Processing Artist file tesfdsf...\n");
		}  catch (JsonParseException e) {
			System.out.println("Error parsing the file.");
		} catch (JsonMappingException e) {
			System.out.println("Error mapping to Java object.");
		} catch (IOException e) {
			System.out.println (e.toString());
		}		
	}
	
	public static void main(String[] args) {
		
		SpringApplication.run(Application.class, args);
		
	}

}
