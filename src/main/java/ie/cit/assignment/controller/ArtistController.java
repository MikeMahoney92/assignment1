package ie.cit.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import ie.cit.assignment.entity.Artist;
import ie.cit.assignment.entity.Artwork;
import ie.cit.assignment.entity.Movements;
import ie.cit.assignment.repository.ArtistRepository;

@Controller
@RequestMapping("/artist")
public class ArtistController {

	@Autowired
	ArtistRepository artistRepository;
	
	@RequestMapping("")
	public String index(Model model) {
		
		List<Artist> artists = artistRepository.findAll();
		model.addAttribute("artists", artists);
		return "artist/artists";
		
	}
	
	@RequestMapping("/viewArtist/{id}")
	public String viewArtist(@PathVariable String id, Model model) {
		
		Artist artist = artistRepository.get(id);
		List <Movements> movement;
		movement = artist.getMovements();
		
		List<Artwork> artwork;
		artwork = artist.getArtwork();
		
		model.addAttribute("artwork", artwork);
		model.addAttribute("artist", artist);
		model.addAttribute("movement", movement);
		return "artist/viewArtist";
		
	}
	
}
