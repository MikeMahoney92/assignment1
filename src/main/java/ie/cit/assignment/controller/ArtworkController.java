package ie.cit.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ie.cit.assignment.entity.Artist;
import ie.cit.assignment.entity.Artwork;
import ie.cit.assignment.entity.Comment;
import ie.cit.assignment.entity.Contributors;
import ie.cit.assignment.entity.Movements;
import ie.cit.assignment.repository.ArtistRepository;
import ie.cit.assignment.repository.ArtworkRepository;

@Controller
@RequestMapping("/artwork")
public class ArtworkController {

	@Autowired
	ArtworkRepository artworkRepository;
	
	ArtistRepository artistRepository;
	
	@RequestMapping("")
	public String index(Model model) {
		
		List<Artwork> artworks = artworkRepository.findAll();
		model.addAttribute("artworks", artworks);
		return "artwork/artworks";
		
	}
	
	@RequestMapping("/viewArtwork/{acno}")
	public String viewArtwork(@PathVariable String acno, Model model) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        model.addAttribute("username", name);
        
        List<Comment> comments = artworkRepository.getComments(acno);
		model.addAttribute("comments", comments);
		
		Artwork artwork = artworkRepository.get(acno);
		Artist artist = artwork.getArtist();
		
		List<Movements> movements = artwork.getMovements();
		
		model.addAttribute("artwork", artwork);
		model.addAttribute("artist", artist);
		model.addAttribute("movement", movements);
		return "artwork/viewArtwork";
		
	}
	
	@RequestMapping(value="/comment", method=RequestMethod.POST)
    public ModelAndView commentSubmit(@RequestParam String userName, @RequestParam String artworkId, @RequestParam String content) {
		
        Comment comment = new Comment();
        
        comment.setUsername(userName);
        comment.setArtworkId(artworkId);
        comment.setContent(content);
        
        artworkRepository.addComment(comment);
        
        return new ModelAndView("redirect:/artwork/viewArtwork/" + artworkId);
    }

}
