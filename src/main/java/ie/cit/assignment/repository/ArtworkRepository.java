package ie.cit.assignment.repository;

import java.util.List;

import javax.sql.DataSource;

import ie.cit.assignment.entity.Artist;
import ie.cit.assignment.entity.Artwork;
import ie.cit.assignment.entity.Comment;
import ie.cit.assignment.entity.Movements;

public interface ArtworkRepository {

	void setDataSource(DataSource dataSource);
	
	Movements findMovement(String movement);
	
	List<Artwork> findAll();
	
	Artwork get(String acno);
	
	void save(Artwork artwork);

	List<Comment> getComments(String artworkId);

	void addComment(Comment comment);
	
}
