package ie.cit.assignment.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import ie.cit.assignment.entity.Artist;
import ie.cit.assignment.entity.Artwork;
import ie.cit.assignment.entity.Comment;
import ie.cit.assignment.entity.Contributors;
import ie.cit.assignment.entity.Movements;

@Repository
public class JdbcArtworkRepository implements ArtworkRepository {

	private JdbcTemplate jdbcTemplate;
	
	private SimpleJdbcInsert insertArtwork;
	private SimpleJdbcInsert insertMovement;
	private SimpleJdbcInsert insertArtworkMovement;
	private SimpleJdbcInsert insertComment;
	
	ArtistRepository artistRepository;
	
	@Autowired
	public JdbcArtworkRepository(JdbcTemplate jdbcTemplate) {
		
		this.jdbcTemplate = jdbcTemplate;
		
	}
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		
		insertArtwork = new SimpleJdbcInsert(dataSource)
				.withTableName("artworks")
				.usingColumns("id", "title","artistId","url");
		insertMovement = new SimpleJdbcInsert(dataSource)
				.withTableName("movements")
				.usingColumns("name","id");
		insertArtworkMovement = new SimpleJdbcInsert(dataSource)
				.withTableName("artwork_movements")
				.usingColumns("artworkId", "movementId");
		insertComment = new SimpleJdbcInsert(dataSource)
				.withTableName("comments")
				.usingColumns("username", "artworkId", "comment");
		
	}
	
	public void save(Artwork artwork) {
		
		try{
			String test1;
			Map<String, Object> artworkParams = new HashMap<String, Object>();
			artworkParams.put("id", artwork.getAcno());
			artworkParams.put("title", artwork.getTitle());
			artworkParams.put("artistId", artwork.getContributors());
			if(artwork.getUrl() != null){
				test1 = artwork.getUrl();
			}else{
				test1 = "No link Available";
			}
			artworkParams.put("url", test1);
			insertArtwork.execute(artworkParams);
			
			Map<String, Object> movementParams = new HashMap<String, Object>();
			Map<String, Object> artworkMovementsParams = new HashMap<String, Object>();
					
			if(artwork.getMovements() != null){
				for (int i= 0; i< artwork.getMovements().size();i++){
					String movementName = artwork.getMovements().get(i).getName();
					int movementId = artwork.getMovements().get(i).getId();
					Movements movement = findMovement(movementName);
					
					if (movement == null){
						movementParams.put("name", movementName);
						movementParams.put("id", movementId);
						insertMovement.execute(movementParams);
						
						System.out.println(movementName);
					}
					artworkMovementsParams.put("artworkId", artwork.getAcno());
					artworkMovementsParams.put("movementId", movementId);
					insertArtworkMovement.execute(artworkMovementsParams);
					artworkMovementsParams.clear();
				}
			}
		}catch (Exception e) {
	     System.out.println(e.toString());
	   }	
	}
	
	public Movements findMovement(String movementName){
		
		try {
			
			String sql = "SELECT * FROM movements WHERE name = ?";
		 
			Movements movement = (Movements)jdbcTemplate.queryForObject(
					sql, new Object[] { movementName }, 
					new BeanPropertyRowMapper<Movements>(Movements.class));
			
			//System.out.println(movement);
			
			return movement;
		
		} catch (EmptyResultDataAccessException e) {
			
			return null;
			
		}	
		
	}

	@Override
	public List<Artwork> findAll() {
		
		String sql = "SELECT * FROM artworks ORDER BY RAND() LIMIT 30";
		

		List<Artwork> artworks = new ArrayList<Artwork>();
		
		try {
			
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
			
			for (Map row : rows) {
				Artwork artwork = new Artwork();
				artwork.setAcno((String)(row.get("id")));
				artwork.setTitle((String)row.get("title"));
				artwork.setArtist1((String)row.get("artistId"));
				artwork.setUrl((String)row.get("url"));
				artwork.setArtistName((String)row.get("artistName"));
				artworks.add(artwork);
			}
			
			return artworks;
		
		} catch (EmptyResultDataAccessException e) {
			
			return null;
			
		}
		
	}

	@Override
	public Artwork get(String acno) {
		
		try {
			
			String sql = "SELECT * FROM artworks WHERE id = ?";
		 
			Artwork artwork = (Artwork)jdbcTemplate.queryForObject(
					sql, new Object[] { acno }, 
					new BeanPropertyRowMapper<Artwork>(Artwork.class));
			artwork.setAcno(acno);
			
			
			/*------------------------------------------------------------------*/
			
			String sql1 = "SELECT * FROM artwork_movements WHERE artworkId ='"+acno+"'";
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql1);
			
			List<Movements> movements = new ArrayList<Movements>();
			String sql2 = "SELECT * FROM movements WHERE id = ?";
			int movementId;
			
			for(Map row : rows){
				Movements movement = new Movements();
				movementId = (int)(row.get("movementId"));
				movement = (Movements)jdbcTemplate.queryForObject(
						sql2, new Object[] { movementId }, 
						new BeanPropertyRowMapper<Movements>(Movements.class));
				movements.add(movement);
			}
			artwork.setMovements(movements);
			
			/*------------------------------------------------------------------*/
			String sql3 ="SELECT artistId FROM artworks WHERE id = '"+acno+"'";
			List<Map<String, Object>> rows1 = jdbcTemplate.queryForList(sql3);
			
			String sql4 = "SELECT * FROM artists WHERE id = ?";
			Artist artist = new Artist();
			String artistId;
			for(Map row1 : rows1){
				artistId = (String)(row1.get("artistId"));
				artist = (Artist)jdbcTemplate.queryForObject(
						sql4, new Object[] { artistId }, 
						new BeanPropertyRowMapper<Artist>(Artist.class));
			}
			artwork.setArtist(artist);
			
			System.out.println(artwork);
			return artwork;
		
		} catch (EmptyResultDataAccessException e) {
			
			return null;
			
		}	
		
	}
	
	@Override
	public void addComment(Comment comment) {
		
		try {
			
			Map<String, Object> commentParams = new HashMap<String, Object>();
			commentParams.put("username", comment.getUsername());
			commentParams.put("artworkId", comment.getArtworkId());
			commentParams.put("comment", comment.getContent());
			
			insertComment.execute(commentParams);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public List<Comment> getComments(String artworkId) {
		
		String sql = "SELECT * FROM comments WHERE artworkId = \"" + artworkId + "\" LIMIT 20";
		
		List<Comment> comments = new ArrayList<Comment>();
		
		try {
			
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
			
			for (Map row : rows) {
				Comment comment = new Comment();
				comment.setUsername((String)(row.get("username")));
				comment.setArtworkId((String)row.get("artworkId"));
				comment.setContent((String)row.get("comment"));
				comments.add(comment);
			}
			
			return comments;
		
		} catch (EmptyResultDataAccessException e) {
			
			return null;
			
		}
	}
	
}
