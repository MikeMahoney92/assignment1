package ie.cit.assignment.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Contributors {
	private int id ;

	@Override
    public String toString() {
        return  String.valueOf(id);
    }
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
