package ie.cit.assignment.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Artwork {

	private String acno;
	
	private String artist1;
	
	@JsonProperty("all_artists")
	private String artistName;

	List<Contributors> contributors;
	List<Movements> movements;
	Artist artist;
	
	private String title;
	
	@JsonProperty("thumbnailUrl")
	private String url;
	
	
	public String toString() {
		String artworkAsString =
				"Title: " + title +
				"\nAcno: " + acno + 
				"\nArtist ID: "+ contributors +
				"\nMovements: "+ movements;
		return artworkAsString;
	}
	
	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}
	
	public List<Movements> getMovements() {
		return movements;
	}

	public void setMovements(List<Movements> movements) {
		this.movements = movements;
	}
	
	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	
	public List<Contributors> getContributors(){
		return contributors;
	}
	
	public void setContributors(List<Contributors> contributors){
		this.contributors = contributors;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getAcno() {
		return acno;
	}
	
	public void setAcno(String acno) {
		this.acno = acno;
	}
	
	public String getArtist1() {
		return artist1;
	}

	public void setArtist1(String artist1) {
		this.artist1 = artist1;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
}
